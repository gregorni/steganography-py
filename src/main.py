import argparse
import sys

from .lib.run import Runner

parser = argparse.ArgumentParser(description="Steganograpy – Hide text in images.")
parser.add_argument(
    "mode",
    choices=["decode", "encode"],
    help="Whether to hide or read text to/from the image.",
)
encode_mode_on = "encode" in sys.argv

parser.add_argument("-i", "--input", type=str, required=True, help="Input filename.")
parser.add_argument(
    "-o", "--output", type=str, required=encode_mode_on, help="Output filename."
)

group = parser.add_mutually_exclusive_group(required=encode_mode_on)
group.add_argument("-t", "--text", type=str, help="Text to hide.")
group.add_argument("-f", "--file", type=str, help="File to read text from.")

args = parser.parse_args()


def text_to_hide():
    if args.text:
        return args.text
    try:
        return open(args.file, "r").read()
    except IOError as err:
        parser.error(f"Could not read text file: {err}")


if encode_mode_on:
    Runner.encode(args.input, args.output, text_to_hide())

else:
    if args.output or args.text or args.file:
        parser.error("Superfluous option: 'decode' mode requires only input option.")
    Runner.decode(args.input)
