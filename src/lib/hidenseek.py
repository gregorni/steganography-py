class HideNSeek:
    @staticmethod
    def induce_txt_bytes(img_bytes, txt_bytes):  # *_bytes should be lists
        img_bytes = list(map(str, img_bytes))
        if len(img_bytes) < len("".join(map(str, txt_bytes))):
            raise Exception("Text could not fit into image")

        txt_bytes = "".join(txt_bytes)

        for i in range(len(txt_bytes)):
            img_bytes[i] = str(img_bytes[i])[:-1] + str(txt_bytes[i])

        return img_bytes  # returns list

    @staticmethod
    def extract_txt_bytes(img_bytes):  # *_bytes should be list
        last_chars = []
        for i in range(len(img_bytes)):
            last_chars.append(str(img_bytes[i])[-1:])

        return last_chars  # returns list of single letter strings
