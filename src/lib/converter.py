import cv2
import numpy


def list_to_bin(list_dec):  # list_dec is a list
    list_bin = []
    for i in list_dec:
        list_bin.append(str(bin(i)[2:]).zfill(8))
    return list_bin  # returns list


class Converter:
    @staticmethod
    def txt_to_bin(txt: str):
        txt_dec = []
        for i in txt:
            txt_dec.append(ord(i))
        return list_to_bin(txt_dec)  # returns list

    @staticmethod
    def bin_to_txt(txt_bin):  # txt_bin is a list of single letter strings
        txt_bin = [
            "".join(txt_bin[i : i + 8]) for i in range(0, len(txt_bin), 8)
        ]  # put single-letter list into 8-letter list

        txt = ""
        for i in txt_bin:
            txt += chr(int(i, 2))
        return txt  # returns string

    @staticmethod
    def img_to_bin(file: str):
        img_bin = []
        img = cv2.imread(file, cv2.IMREAD_UNCHANGED)
        for i in img.flat:
            img_bin.append(str(bin(i)[2:]).zfill(8))
        return (
            img_bin,
            img.shape,
        )  # returns a list of 8-letter strings and a tuple with 3 integers

    @staticmethod
    def bin_to_img(
        img_bin, file: str, shape
    ):  # img_bin is a list, shape is a tuple with 3 integers
        img_bin_int = []
        for i in img_bin:
            img_bin_int.append(int(i, 2))

        array = numpy.array(img_bin_int)
        array.shape = shape
        cv2.imwrite(file, array)
