from .converter import Converter
from .hidenseek import HideNSeek


class Runner:
    @staticmethod
    def encode(input_image: str, output_image: str, text: str):
        txt_bin = Converter.txt_to_bin(text)
        img_bin, img_shape = Converter.img_to_bin(input_image)
        altered_img = HideNSeek.induce_txt_bytes(img_bin, txt_bin)

        Converter.bin_to_img(altered_img, output_image, img_shape)

    @staticmethod
    def decode(input_image: str):
        img_bin = Converter.img_to_bin(input_image)[0]
        extracted_txt_bin = HideNSeek.extract_txt_bytes(img_bin)
        extracted_txt_real = Converter.bin_to_txt(extracted_txt_bin)
        print(extracted_txt_real)
