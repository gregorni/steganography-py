# Steganography-Py

A tool to hide text in an image, using the least-significant-bit method.
Written in Python.

## Usage

### Flags

#### --input

```bash
# Define the input file.
--input [INPUT]
# or
-i [INPUT]
```

#### --output

```bash
# Define the output file.
--output [OUTPUT]
# or
-o [OUTPUT]
```

#### --text

```bash
# Define the text to be hidden.
--text [TEXT]
# or
-t [TEXT]
```

#### --file

```bash
# Read the text to be hidden from a text file.
--file [FILE]
# or
-f [FILE]
```

### Usage Examples

#### Encode

```bash
# Encode a string of text
python src/main.py encode --input input.png --output output.png --text "Hello, World!"
```

```bash
# Encode the contents of a text file
python src/main.py encode -i input.png -o output.png -f hello_world.txt
```

#### Decode

```bash
# Read the encoded string in an image
python src/main.py decode --input input.png
```

## Example images

### Input image

<div align="center">
  <img src=https://codeberg.org/gregorni/steganography-py/raw/branch/main/images/Fractal.png>
</div>

### Output Image with 2800 words of Lorem ipsum embedded

<div align="center">
  <img src=https://codeberg.org/gregorni/steganography-py/raw/branch/main/images/Fractal_out.png>
</div>
